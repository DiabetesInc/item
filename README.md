[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/uses-badges.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/does-not-contain-treenuts.svg)](https://forthebadge.com)

# Product

This is a case study application for the myRetail Restful Service problem

### Requirements

* Java 8
* Docker(and docker-compose) 

### Tools

* Micronaut - Http Framework 
  * Quick startup time 
  * Less bloat than heavier frameworks like Spring
 
* DynamoDB - Amazon NoSQL datastore 
  * Simple key-value store of JSON blobs
  * complex filtering on attributes in those blobs
 
* Retrofit - HTTP Client
  *  Abstracts away HTTP to make code simpler
  * Integration testing of controller classes!
 
* Localstack - AWS Emulator
  * Lightweight
  * Integration testing against DynamoDB  
 
 ### Build + Run
 
 `docker-compose up -d` will bring up a localstack instance
 
 `./gradlew build` will compile and run tests(integration tests will fail unless you bring up the localstack container in docker)
 
 `./gradlew run` will bring up a locally running instance(likely on port 8080)
 
 
 ### Quick API documentation
 
 The app has 2 endpoints, a PUT and a GET
 
 #### PUT: /products/v1/products/{id}
 
 Body: 
 
 ```
{
    "value": "12.98",
    "currency_code": "USD"
}
 ```
 
 Response:
 ```
 {
   "id": "{id}",
   "current_value": 12.98,
   "currency_code": "USD"
 }
 ```
 
 #### GET: /products/v1/products/{id}
 Response:
 ```
 {
   "id": "{id}",
   "name": "Halloween Sinister Pumpkin Fogger Fog Machine",
   "current_price": {
     "current_value": 12.98,
     "currency_code": "USD"
   }
 }
 ```
 
 ### TODO
 
 If this was actually being deployed for real, we would setup the CD part of the 
 CI/CD pipeline to get it deploying out to dev. As this is just a POC we will not 
 be taking the time to do that.
 * Build jar into a docker image for deployment
 * Add auto versioning
 * Push artifacts to a remote repository(artifactory?)
 * Auto deploy to a dev environment upon merge to master
 
 