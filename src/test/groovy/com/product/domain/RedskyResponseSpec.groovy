package com.product.domain

import com.fasterxml.jackson.databind.ObjectMapper
import com.product.factory.ObjectMapperBuilder
import spock.lang.Specification

class RedskyResponseSpec extends Specification {

    ObjectMapper mapper = ObjectMapperBuilder.build()

    void 'I can marshal a redsky response'() {
        given:
        String text = this.class.getResource('/redsky_response.json').text

        when:
        RedskyResponse response = mapper.readValue(text, RedskyResponse)

        then:
        response.product.item.productDescription.title == 'Halloween Sinister Pumpkin Fogger Fog Machine'
        response.product.item.tcin == '14247809'
    }
}
