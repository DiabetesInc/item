package com.product.domain

import spock.lang.Specification

class PriceSpec extends Specification {

    void 'Only Update values we have received, leave the nulls'() {
        given:
        Price existing = new Price(
            id: UUID.randomUUID(),
            currentValue: 9.99,
            currencyCode: CurrencyCode.USD
        )

        PriceUpdateRequest request = new PriceUpdateRequest(
            value: 11.50,
            currencyCode:  null
        )

        when:
        existing.updateFromRequest(request)

        then:
        existing.currencyCode == CurrencyCode.USD
        existing.currentValue == request.value
    }
}
