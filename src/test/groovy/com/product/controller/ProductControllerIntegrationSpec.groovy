package com.product.controller

import com.product.api.ProductApi
import com.product.api.RetrofitClientBuilder
import com.product.domain.CurrencyCode
import com.product.domain.Price
import com.product.domain.PriceUpdateRequest
import com.product.domain.ProductResponse
import io.micronaut.context.ApplicationContext
import io.micronaut.runtime.server.EmbeddedServer
import retrofit2.HttpException
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class ProductControllerIntegrationSpec extends Specification {

    @Shared
    @AutoCleanup
    EmbeddedServer server

    @Shared
    ProductApi api

    //This may die someday, in which case these tests will also die :(
    @Shared
    String id = '13860428'

    void setupSpec() {
        server = ApplicationContext.run(EmbeddedServer)
        api = new RetrofitClientBuilder()
                .withBaseUrl(server.URL.toExternalForm())
                .build()
                .create(ProductApi)
    }

    void 'Put a price object'() {
        given:
        String randomId = UUID.randomUUID()
        PriceUpdateRequest request = new PriceUpdateRequest(value: 12.34, currencyCode: CurrencyCode.USD)

        when:
        Price price = api.updatePrice(randomId, request).blockingSingle()

        then:
        price.id == randomId
        price.currencyCode == request.currencyCode
        price.currentValue == request.value
    }

    void 'get a product with no pricing(this will fail if you PUT a pricing for this TCIN first)'() {
        when:
        ProductResponse response = api.getProduct(id).blockingSingle()

        then:
        response.id == id
        response.name
        !response.currentPrice
    }

    void 'get a product with pricing'() {
        given:
        String id = '50851405'
        PriceUpdateRequest request = new PriceUpdateRequest(value: 12.34, currencyCode: CurrencyCode.USD)

        when:
        api.updatePrice(id, request).blockingSingle()
        ProductResponse response = api.getProduct(id).blockingSingle()

        then:
        response.currentPrice
    }

    void '404 on unknown product'() {
        given:
        String fake = UUID.randomUUID()

        when:
        api.getProduct(fake).blockingSingle()

        then:
        HttpException e = thrown HttpException

        and:
        e.code() == 404
    }
}
