package com.product.repository

import com.product.domain.CurrencyCode
import com.product.domain.Price
import com.product.repository.config.DynamoConfig
import spock.lang.Specification

class PriceRepositoryIntegrationSpec extends Specification {

    DynamoConfig config = new DynamoConfig()
    PriceRepository repository

    void setup() {
        repository = new PriceRepository(config)
    }

    void 'I can save a price and get it back!'() {
        given:
        Price price = new Price(
            id: UUID.randomUUID(),
            currentValue: 12.34,
            currencyCode: CurrencyCode.USD
        )

        when:
        repository.savePrice(price)
        Price returned = repository.getPrice(price.id)

        then:
        returned == price
    }
}
