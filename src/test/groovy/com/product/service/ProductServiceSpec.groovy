package com.product.service

import com.product.api.RedskyApi
import com.product.domain.Product
import com.product.domain.ProductDescription
import com.product.domain.RedskyItem
import com.product.domain.RedskyProduct
import com.product.domain.RedskyResponse
import com.product.exception.NotFoundException
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import spock.lang.Specification

class ProductServiceSpec extends Specification {

    ProductService service
    RedskyApi api

    void setup() {
        api = Mock()
        service = new ProductService(api)
    }

    void 'Call redsky, map it to our internal model'() {
        given:
        String id = '12046362'
        RedskyResponse response = new RedskyResponse(
            product: new RedskyProduct(
                item: new RedskyItem(
                    productDescription: new ProductDescription(title: 'Tiki Head Bubble Machine'),
                    tcin: '12046362'
                )
            )
        )

        when:
        Product product = service.getProduct(id)

        then:
        1 * api.requestProductData(id) >> Observable.just(response)
        0 * _

        and:
        product.id == response.product.item.tcin
        product.title == response.product.item.productDescription.title
    }

    void 'When Redsky 404s, throw a NotFoundException'() {
        given:
        String id = 'badId'
        Response response = Response.error(404, ResponseBody.create(null, 'error'))

        when:
        service.getProduct(id)

        then:
        1 * api.requestProductData(id) >> { throw new HttpException(response) }
        NotFoundException e = thrown NotFoundException
        0 * _

        and:
        e.message == "Unable to find item with id ${id}"
    }

    void 'If Redsky does anything besides 404 or 200, just rethrow the error'() {
        given:
        String id = 'badId'
        Response response = Response.error(400, ResponseBody.create(null, 'error'))

        when:
        service.getProduct(id)

        then:
        1 * api.requestProductData(id) >> { throw new HttpException(response) }
        0 * _

        and:
        thrown HttpException
    }
}
