package com.product.service

import com.product.domain.CurrencyCode
import com.product.domain.Price
import com.product.domain.PriceUpdateRequest
import com.product.repository.PriceRepository
import spock.lang.Specification

class PriceServiceSpec extends Specification {

    PriceService service
    PriceRepository repository

    void setup() {
        repository = Mock()
        service = new PriceService(repository)
    }

    void 'I can save a new price object'() {
        given:
        String id = UUID.randomUUID()
        PriceUpdateRequest request = new PriceUpdateRequest(
            value: 12.34,
            currencyCode: CurrencyCode.USD
        )

        Price expectedPrice = new Price(
            id: id,
            currentValue: request.value,
            currencyCode: request.currencyCode
        )

        when:
        service.updatePrice(request, id)

        then:
        1 * repository.getPrice(id) >> null
        1 * repository.savePrice(expectedPrice) >> expectedPrice
        0 * _
    }

    void 'I can update an existing object'() {
        given:
        String id = UUID.randomUUID()
        PriceUpdateRequest request = new PriceUpdateRequest(
            value: 12.34,
            currencyCode: CurrencyCode.USD
        )

        Price existingPrice = new Price(
            id: id,
            currentValue: 9.99,
            currencyCode: CurrencyCode.USD
        )

        when:
        service.updatePrice(request, id)

        then:
        1 * repository.getPrice(id) >> existingPrice
        1 * repository.savePrice(_ as Price) >> { Price p ->
            assert p.id == existingPrice.id &&
                    p.currentValue == request.value &&
                    p.currencyCode == request.currencyCode
        }
    }
}
