package com.product.domain

import com.amazonaws.services.dynamodbv2.document.Item
import groovy.transform.Canonical

@Canonical
class Price {

    String id

    BigDecimal currentValue

    CurrencyCode currencyCode

    Item toDynamoItem() {
        new Item()
            .withPrimaryKey('id', id)
            .with('current_value', currentValue)
            .with('currency_code', currencyCode.toString())
    }

    Price fromDynamoItem(Item item) {
        this.id = item.getString('id')
        this.currentValue = new BigDecimal(item.getString('current_value'))
        this.currencyCode = CurrencyCode.valueOf(item.getString('currency_code'))
        return this
    }

    Price updateFromRequest(PriceUpdateRequest request) {
        this.currentValue = request.value ?: currentValue
        this.currencyCode = request.currencyCode ?: currencyCode
        return this
    }

}
