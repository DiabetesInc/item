package com.product.domain

import groovy.transform.Canonical

@Canonical
class PriceResponse {

    BigDecimal currentValue

    CurrencyCode currencyCode

    PriceResponse fromPrice(Price price) {
        this.currentValue = price.currentValue
        this.currencyCode = price.currencyCode
        this
    }
}
