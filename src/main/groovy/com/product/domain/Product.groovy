package com.product.domain

class Product {

    String title
    String id

    Product fromProductResponse(RedskyResponse response) {
        new Product(
            title: response.product.item.productDescription.title,
            id: response.product.item.tcin
        )
    }
}
