package com.product.domain

import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical

@Canonical
class ProductResponse {

    String id
    String name

    @JsonProperty('current_price')
    PriceResponse currentPrice

    ProductResponse fromProductAndPrice(Product product, Price price) {
        this.id = product.id
        this.name = product.title
        this.currentPrice = new PriceResponse().fromPrice(price)
        this
    }

    ProductResponse fromProduct(Product product) {
        this.id = product.id
        this.name = product.title
        this
    }
}
