package com.product.domain

import javax.validation.constraints.Positive

class PriceUpdateRequest {

    @Positive
    BigDecimal value

    CurrencyCode currencyCode

    Price toPrice(String id) {
        new Price(
            id: id,
            currentValue: value,
            currencyCode: currencyCode
        )
    }
}
