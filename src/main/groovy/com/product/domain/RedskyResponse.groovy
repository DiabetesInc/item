package com.product.domain

import groovy.transform.Canonical

@Canonical
class RedskyResponse {
    RedskyProduct product
}
