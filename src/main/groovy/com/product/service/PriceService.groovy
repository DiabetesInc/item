package com.product.service

import com.product.domain.Price
import com.product.domain.PriceUpdateRequest
import com.product.repository.PriceRepository
import groovy.util.logging.Slf4j

import javax.inject.Singleton

@Singleton
@Slf4j
class PriceService {

    PriceRepository repository

    PriceService(PriceRepository repository) {
        this.repository = repository
    }

    Price getPrice(String id) {
        repository.getPrice(id)
    }

    //Technically this is an upsert, if the price doesn't exist, we're just going to insert the new record
    Price updatePrice(PriceUpdateRequest request, String id) {
        log.info("Got pricing update request for id: ${id}, with update: ${request}")
        Price priceToUpdate = request.toPrice(id)

        Price existingPrice = repository.getPrice(id)

        if (existingPrice) {
            priceToUpdate = existingPrice.updateFromRequest(request)
        }

        log.info("Saving: " + priceToUpdate)
        repository.savePrice(priceToUpdate)
    }
}
