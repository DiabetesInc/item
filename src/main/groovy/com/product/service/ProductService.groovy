package com.product.service

import com.product.api.RedskyApi
import com.product.domain.Product
import com.product.domain.RedskyResponse
import com.product.exception.NotFoundException
import retrofit2.HttpException

import javax.inject.Singleton

@Singleton
class ProductService {

    RedskyApi redskyApi

    ProductService(RedskyApi redskyApi) {
        this.redskyApi = redskyApi
    }

    Product getProduct(String id) {
        RedskyResponse response
        try {
            response = redskyApi.requestProductData(id).blockingSingle()
        } catch(HttpException e) {
            throw(e.code() == 404 ? new NotFoundException("Unable to find item with id ${id}") : e)
        }
        new Product().fromProductResponse(response)
    }
}
