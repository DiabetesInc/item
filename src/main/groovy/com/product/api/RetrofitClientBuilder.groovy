package com.product.api

import com.product.factory.ObjectMapperBuilder
import okhttp3.OkHttpClient.Builder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


class RetrofitClientBuilder {

    String baseUrl

    Retrofit build() {
        new Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(new Builder().build())
            .addConverterFactory(
                JacksonConverterFactory.create(
                    ObjectMapperBuilder.build()
                )
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    RetrofitClientBuilder withBaseUrl(String url) {
        this.baseUrl = url
        this
    }
}
