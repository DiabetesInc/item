package com.product.api

import groovy.transform.AutoClone

@AutoClone
class RedskyConfig {
    String baseUrl = 'https://redsky.target.com/'
}
