package com.product.api

import com.product.domain.Price
import com.product.domain.PriceUpdateRequest
import com.product.domain.ProductResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ProductApi {

    @GET('products/v1/products/{id}')
    Observable<ProductResponse> getProduct(@Path('id') String id)

    @PUT('products/v1/products/{id}')
    Observable<Price> updatePrice(@Path('id') String id, @Body PriceUpdateRequest request)
}
