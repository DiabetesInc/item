package com.product.api

class RedskyProvider {

    static RedskyApi get(RedskyConfig config) {
            new RetrofitClientBuilder()
            .withBaseUrl(config.baseUrl)
            .build()
            .create(RedskyApi)
    }
}
