package com.product.api

import com.product.domain.RedskyResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface RedskyApi {

    @GET('v2/pdp/tcin/{id}?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics')
    Observable<RedskyResponse> requestProductData(@Path('id') String id)
}
