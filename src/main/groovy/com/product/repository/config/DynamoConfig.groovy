package com.product.repository.config

import groovy.transform.AutoClone

@AutoClone
class DynamoConfig {

    String tableName = 'Price'
    String serviceUrl = 'http://localhost:4569'
    String hashKey = 'id'
}
