package com.product.repository

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement
import com.amazonaws.services.dynamodbv2.model.KeyType
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType
import com.product.domain.Price
import com.product.repository.config.DynamoConfig

class PriceRepository {

    private final String tableName
    private DynamoDB dynamoDB
    private Table table

    PriceRepository(DynamoConfig config) {
        this.tableName = config.tableName
        init(config)
        if (!listTables().contains(tableName)) {
            createTable(config.hashKey, tableName)
        }
    }

    Price getPrice(String id) {
        Item item = table.getItem('id', id)
        if (item) {
            return new Price().fromDynamoItem(item)
        }
        null
    }

    Price savePrice(Price price) {
        table.putItem(price.toDynamoItem())
        price
    }

    void createTable(String hashKey, String tableName) {
        AttributeDefinition keyAttribute = new AttributeDefinition(hashKey, ScalarAttributeType.S)
        KeySchemaElement keySchemaElement = new KeySchemaElement(hashKey, KeyType.HASH)

        CreateTableRequest createTableRequest = new CreateTableRequest()
            .withTableName(tableName)
            .withKeySchema(keySchemaElement)
            .withAttributeDefinitions(keyAttribute)
            .withProvisionedThroughput(new ProvisionedThroughput()
                .withReadCapacityUnits(10L)
                .withWriteCapacityUnits(5L))

        Table table = dynamoDB.createTable(createTableRequest)
        table.waitForActive()
    }

    List<String> listTables() {
        dynamoDB.listTables().collect { Table result -> result.tableName }
    }

    void init(DynamoConfig config) {
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
        if (config.serviceUrl) {
            builder.withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration(config.serviceUrl, 'us-east-2')
            )
        }
        AmazonDynamoDB amazonDynamoDB = builder.build()
        dynamoDB = new DynamoDB(amazonDynamoDB)
        table = dynamoDB.getTable(tableName)
    }
}
