package com.product.factory

import com.product.repository.PriceRepository
import com.product.repository.config.DynamoConfig
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory

import javax.inject.Singleton

@Factory
class PriceRepositoryFactory {

    @Bean
    @Singleton
    PriceRepository getPriceRepository(DynamoConfig config) {
        new PriceRepository(config)
    }
}
