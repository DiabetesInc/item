package com.product.factory

import com.edgescope.config.EnvConfigLoader
import com.product.api.RedskyConfig
import com.product.repository.config.DynamoConfig
import io.micronaut.context.annotation.Factory

import javax.inject.Singleton

@Factory
class ConfigFactory {

    @Singleton
    DynamoConfig getDynamo() {
        EnvConfigLoader.overrideFromEnvironment(new DynamoConfig(), 'DYNAMO')
    }

    @Singleton
    RedskyConfig getRedsky() {
        EnvConfigLoader.overrideFromEnvironment(new RedskyConfig(), 'REDSKY')
    }
}
