package com.product.factory

import com.product.api.RedskyApi
import com.product.api.RedskyConfig
import com.product.api.RedskyProvider
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory

import javax.inject.Singleton

@Factory
class ApiFactory {

    @Bean
    @Singleton
    RedskyApi getRedskyApi(RedskyConfig config) {
        RedskyProvider.get(config)
    }
}
