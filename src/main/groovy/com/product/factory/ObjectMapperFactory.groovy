package com.product.factory

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Replaces
import io.micronaut.jackson.JacksonConfiguration
import io.micronaut.jackson.ObjectMapperFactory
import javax.annotation.Nullable

import javax.inject.Singleton

@Factory
@Replaces(ObjectMapperFactory)
class ProductObjectMapperFactory extends ObjectMapperFactory {

    @Override
    @Singleton
    @Replaces(ObjectMapper)
    ObjectMapper objectMapper(@Nullable JacksonConfiguration jacksonConfiguration,
                              @Nullable JsonFactory jsonFactory) {
        ObjectMapperBuilder.build()
    }
}