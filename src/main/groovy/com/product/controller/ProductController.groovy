package com.product.controller

import com.fasterxml.jackson.core.JsonParseException
import com.product.domain.Price
import com.product.domain.PriceUpdateRequest
import com.product.domain.Product
import com.product.domain.ProductResponse
import com.product.exception.NotFoundException
import com.product.service.PriceService
import com.product.service.ProductService
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Error
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.Put
import io.micronaut.http.hateoas.JsonError
import io.micronaut.http.hateoas.Link
import io.micronaut.validation.Validated

import javax.validation.Valid

@Slf4j
@CompileStatic
@Controller('/products/v1')
class ProductController {

    ProductService productService
    PriceService priceService

    ProductController(
        ProductService productService,
        PriceService priceService) {
        this.productService = productService
        this.priceService = priceService
    }

    @Get('products/{id}')
    @Produces(MediaType.APPLICATION_JSON)
    ProductResponse getProduct(String id) {
        Product product = productService.getProduct(id)
        Price price = priceService.getPrice(id)
        if (price) {
            return new ProductResponse().fromProductAndPrice(product, price)
        }
        return new ProductResponse().fromProduct(product)
    }

    @Validated
    @Put('products/{id}')
    @Produces(MediaType.APPLICATION_JSON)
    Price updatePrice(@Valid @Body PriceUpdateRequest request, String id) {
        priceService.updatePrice(request, id)
    }

    @Error(global = true)
    HttpResponse<JsonError> jsonError(HttpRequest request, JsonParseException jsonParseException) {
        JsonError error = new JsonError("Invalid JSON: " + jsonParseException.getMessage())
                .link(Link.SELF, Link.of(request.getUri()))

        return HttpResponse.<JsonError>status(HttpStatus.BAD_REQUEST, "Fix Your JSON")
                .body(error)
    }

    @Error(global = true)
    HttpResponse<JsonError> itemNotFound(HttpRequest request, NotFoundException notFoundException) {
        JsonError error = new JsonError("Item not found: " + notFoundException.getMessage())
                .link(Link.SELF, Link.of(request.getUri()))

        return HttpResponse.<JsonError>status(HttpStatus.NOT_FOUND, "Item not found")
                .body(error)
    }
}
